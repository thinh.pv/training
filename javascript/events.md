# Javascript Events

## Body scroll lock

Trong một số các interactive của website, cần lưu ý tới tình huống cần body scroll lock.

**Các case phổ biến:**

- Bật slideout menu trên mobile
- Bật popup trên mobile, desktop

Package sử dụng trong Javascript: [body-scroll-lock](https://www.npmjs.com/package/body-scroll-lock)

**Notes:**

- Khi resize, chủ động điều chỉnh interactive với người dùng: hoặc là ĐÓNG UI, hoặc là UPDATE UI để không bị vỡ layout hay gây ra tình huống  body scroll vẫn đang lock.

Ví dụ:

Slideout mobile menu mở, sau đó khách chuyển sang khung tablet/desktop.
Cần:
- Đóng slideout menu
- Body không bị lockscroll

```javascript
const update = () => {}
on('resize', throttle(update, 300), window)
```