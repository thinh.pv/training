# Xác định height của đối tượng

Lỗi Firefox không nhận height do sử dụng clientHeight

Cách fix: Sử dụng `offsetHeight` song song.

```javascript
const heroHeight = heroEl.clientHeight || heroEl.offsetHeight
```

**Tham khảo:**

- https://stackoverflow.com/questions/5041957/clientheight-in-firefox