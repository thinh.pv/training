# Scroll bug trên mobile khi tap vào input element

Khi tap vào input nằm trong sticky element (top hoặc bottom), user sẽ bị cuốn tận xuống cuối trang. Đây là khuyết điểm của iOS Safari mà mình đã từng gặp phải với SmartyPants Vitamin.

Thường `window.scrollTo(0, x)` sẽ cần add tính offset `x` = 0 bằng việc sử dụng element là `document.body` thì mới hoạt động trên các mobile devices.

- `document.body.offsetTop()` works
- `docment.offsetTop()` works in Desktop only.

**Code fix:**

```javascript
    var $body = $('body'),
        $bar = $(el), // change to your fixed block
        $input = $body.find('input'), // Change it to input inside fixed block
        iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
        iOSTop = 0;
// fix scrolling on fixed input on iOS Safari
    if (iOS) {
        // on touchstart, make the bar absolute to prevent scrolling, set the top position so that
        // the field is always visible
        $input.on('touchstart', function(e) {
            iOSTop = $bar.offset().top;
            $bar.css({
                'position': 'absolute',
                'top': iOSTop,
                'bottom': 'auto'
            });
        });

        // prevent normal touchend otherwise links behind the original input position will trigger
        $input.on('touchend', function(e) {
            e.preventDefault();
            $(this).focus();
        });

        // scroll the viewport top edge to match with the CTA top edge
        // this is to ensure the whole CTA is visible
        $input.on('focus', function(e) {
            setTimeout(function() {
                $('html, body').animate({
                   'scrollTop': $bar.offset().top
                }, 300);
            }, 500);
        });

        // restore the CTA when blurred
        $input.on('blur', function() {
            $bar.css({
                'position': '',
                'top': '',
                'bottom': ''
            })
        });
    }

```
