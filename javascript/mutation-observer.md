`MutationObserver` API is used to respond to a specific change to the DOM. This API is incredibly useful when looking for child nodes of an element to be added or removed by third-party apps like [Showcase](https://apps.shopify.com/shop-instagram) or [Yotpo](https://apps.shopify.com/yotpo-social-reviews).

The following example demonstrates how to use `MutationObserver` API to wait for Showcase content markup fully loaded and init Flickity right after that.

```js
if ((typeof window.Showcase) === 'undefined') { window.Showcase = {} }
  window.Showcase.init = () => {
    const carouselEl = select('.showcase-grid-items', el)
    if (!carouselEl) {
      return
    }

    const observer = new window.MutationObserver((mutations) => {
      if (mutations.length) {
        let flkty = new Flickity(carouselEl, {
          contain: true,
          pageDots: false,
          prevNextButtons: false,
          wrapAround: true
        })
      }
    })

    observer.observe(carouselEl, {
      childList: true
    })
  }
```

After Showcase widget has been initialized, I create a new `MutationObserver` instance using the MutationObserver() constructor. The argument of the constructor is a callback function to initialize a Flickity carousel when new markup is added to `carouselEl` element.

The `observe()` method configures the `MutationObserver` instance begin observing DOM changes. It takes two arguments: the target element (`carouselEl`), and an options object for defining configuration for the observer. By setting `childList` to `true`, the observer will look for changes to child elements of the `carouselEl` element.
