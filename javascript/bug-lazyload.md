# Ảnh trong slideshow sử dụng Flickity không hiển thị

Lỗi này được nhận diện trên 1 số project của Barrel khi sử dụng module `image` có chứa `Layzr.js`.

Trong `image.js`, cần cập nhật để có thêm và export riêng ra

```
const updateLazyLoad = () => instance.update().check()
updateLazyLoad().handlers(true)

export default {} // Default của module, nếu có rồi thì để nguyên không sửa gì vào đây.

export {
  updateLazyLoad
}
```

Trong module gặp lỗi không load ảnh (vd `hero-homepage.js`), thêm đoạn code sau:

```
import { updateLazyLoad } from 'modules/image/image'

    // Sau khi slider được khởi tạo
    const slider = new Flickity(list, options)
    const resize = () => addClass(INIT_CLASS, el)
    const reset = () => removeClass(INIT_CLASS, el)
    const handler = () => {
      reset()
      slider.resize()
      resize()
      updateLazyLoad() // Thêm vào đây
    }
    resize()
    on('resize', handler, window)
    on('load', handler, window) // Nó hoạt động ngay khi load event
```

