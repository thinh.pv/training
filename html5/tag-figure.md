# Tag `<figure>`

Tag này cần sử dụng nếu bên trong là media, ví dụ image, video, code mẫu hay file svg.

Bên trong tag này có thể dùng `<figcaption>` nếu cần mô tả thuộc tính (ví dụ ảnh có mô tả).

Lưu ý là thẻ <figcaption> chỉ nằm đầu tiên hoặc cuối cùng bên trong.

**Tài liệu liên quan**

- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figure
- https://www.w3schools.com/tags/tag_figure.asp