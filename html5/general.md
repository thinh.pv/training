# HTML5 | Markup

## 1. Thứ tự thuộc tính

1. Các thuộc tính trong markup cần được sắp xếp theo thứ tự ưu tiên như sau:

1. `class`
2. `id`, `name`
3. `data-*`
4. `src`, `for`, `type`, `href`, `value`
5. `title`, `alt`
6. `role`, `aria-*`

Ví dụ:

```html5
<a class="..." id="..." data-toggle="modal" href="#">
<input class="form-control" type="text">
```

2. Nếu số lượng thuộc tính lớn hơn màn hình code editor, xuống dòng cho mỗi data-attribute/thuộc tính kể từ thuộc tính thứ hai. Thường thuộc tính cùng dòng tag là `class`

```html5
<section class="hero"
  id="hero-homepage"
  data-modal-target=".modal-hero"
  data-scroll-position=".footer"
>
```

## 2. Self-close tag

> HTML5 không yêu cầu các self endtag cho các tag mở không đóng như `<img>`, `<hr>`,...

```html5
<img class="" alt="" src="">
<hr class="">
```

## 3. Global element

1. Trong website thường có những global class về spacing hoặc layout được quy ước nhằm giúp đảm bảo khoảng cách đều nhau giữa các module. Lưu ý sử dụng nó khi thấy có thể áp dụng.

```css
.container {}
.section {}
.section-bg {}
.section-gutter {}
.section-header {}
.section-footer {}
```

2. Nếu muốn ghi đè các thuộc tính này, sử dụng class của module.

```postcss
/** layout.css **/
.section {
  padding: 40px 0;
}

/** .two-up.css **/
.two-up {
  @media (--l) {
    padding: 60px 0;
  }
}
```

3. Một số project có thể thấy sử dụng tạo nhiều class extend global thì tận dụng lại nếu phù hợp.

```postcss
.container {}
.container--m {
  max-width: var(--container-m);
}
```

## 4. Hiển thị viết hoa

Không viết `TEXT` mà viết `Text` và dùng CSS `uppercase` trong markup. Sử dụng global typography class nếu có thể. 

```html5
<!-- Incorrect -->
<span class="hero__text">HERO TITLE</span>

<!-- Correct -->
<span class="hero__text secondary-heading">Hero Title</span>
```

## 5. Sử dụng Unicode character thay vì nhập tay.

- [Bảng tra danh sách kí tự unicode](https://www.w3schools.com/charsets/ref_utf_punctuation.asp)
- [HTML Symbols](https://www.w3schools.com/html/html_symbols.asp)

Ví dụ:

`&copy;` thay vì nhập `©` trong markup.
`&hellip;` thay vì nhập `...` trong markup.