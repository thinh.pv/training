# HTML5 | Composite Classes

Composite Style là cách viết CSS class bằng cách sử dụng các helper class được tạo ra.

Lấy ví dụ:

- Với project sử dụng cách thường, ta hay thường sử dụng extend, include,...

```postcss
.hero__headline {
  @extend .h2;
}
```

Nhưng với cách viết composite style, trong markup sẽ chứa các class global:

```html
<h2 class="h2 red-text with-underline hero__headline"></h2>
```

Cách viết composite hiện tại đang áp dụng cho Typography trên nhiều project, và quy ước theo từng project.