# Cách Sử dụng `<symbol>` trong `<svg>`

## Lợi thế khi sử dụng icon svg:
- Thay đổi kích thước to, nhỏ mà không làm mất đi chất lượng của ảnh.
- Nhìn sắc nét hơn trên màn hình Retina.
- Kích thước của file cũng nhỏ hơn.

## Lợi thế khi sử dụng thẻ `<symbol>` kết hợp với `<svg>`:
- Viewbox có thể được define từ thẻ `<symbol>` nên không cần gọi lại ở markup. Thuận tiện và ít lỗi vặt hơn
- Dễ dàng sử dụng thẻ `<title>` và `<desc>` hỗ trợ ADA
- Có thể bỏ được `<defs>`

## Cách sử dụng
- Bọc các elements của icon vào bên trong thẻ `<symbol>` với viewbox được define sẵn như bên dưới:
```html
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  
  <symbol id="shape-icon-1" viewBox="200 0 190 800">
    <title>Hỗ trợ ADA</title>
    <desc>Hỗ trợ ADA</desc>
    <!-- <path>s and whatever other shapes in here -->  
  </symbol>
  
  <symbol id="shape-icon-2" viewBox="0 20 100 50">
    <title>Hỗ trợ ADA</title>
    <desc>Hỗ trợ ADA</desc>
    <!-- <path>s and whatever other shapes in here -->  
  </symbol>
  
</svg>
```

- Cách để gọi icon:
```html
<svg class="icon">
  <use xlink:href="#shape-icon-1" />
</svg>

<svg class="icon">
  <use xlink:href="#shape-icon-2" />
</svg>
```

## Chú ý:
- Toàn bộ DOM bên trong thẻ `<use>` được gọi là `shadow DOM` - hoạt động độc lập so với `Virtual DOM`. Do đó các style hay javascript từ bên ngoài không thể tác động vào trong Shadow DOM được và ngược lại.
- Trong một số trường hợp, nếu cần làm animation cho SVG hay style cho từng path một cách linh hoạt thì sử dụng `<svg>` bình thường.
