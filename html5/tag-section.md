# Tag `<section>`

Tag `<section>` cung cấp cấu trúc liên kết HTML chặt hơn so với `<div>`. Mỗi `<section>` thường bao gồm một hoặc nhiều hơn các Heading H1-H6 và các nội dung biểu đạt liên quan.

**Quy ước bổ sung:**
- Nếu là Hero trên đầu trang: thẻ Heading đầu tiên sử dụng `<h1>`, đặt tên class là `__title`
- Nếu là các section khác: thẻ Heading đầu tiên sử dụng `<h2>`, đặt tên class là `__headline`
- Nếu chỉ có Heading mà không có paragraph hay các nội dung khác, ~~không nên sử dụng thẻ `<section>`~~.

*Ví dụ:*

```
<section class="cta-block">
  <h2 class="cta-block__headline">Pink Flamingos</h2>
  <p class="cta-block__description">Plastic flamingos have been used as garden ornaments since the late 1950s.</p>
</section>
```

```
<section class="hero">
  <div class="hero__wrapper">
    <h1 class="hero__title">Let's dance together!</h1>
    <p class="hero__description">We designed a dancing program in all cities.</p>
  </div>
</section>
```

## Tài liệu liên quan

+ https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section
+ https://html.com/tags/section/