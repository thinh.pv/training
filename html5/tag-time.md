# Tag `<time>`

Tag này dùng để mô tả thời gian, bao gồm từ giờ, phút, giây cho tới ngày tháng năm.

## Cú pháp

1. Sử dụng đơn giản nhất là wrap trong cặp thẻ.

```
<p>The year of his birth was <time>1994</time>.</p>
```

2. Sử dụng kèm theo data-attribute

```
<p>Our party is going to be <time datetime="2018-08-11 17:00">the eleventh of August at 5:00pm</time></p>
```

2.1. Sử dụng không có giờ, mà có ngày-tháng-năm

```
<time datetime="2018-08-23">August 23, 2018</time>
<time datetime="2018-08">August 23</time>
<time datetime="2018">2018</time>
```

2.2. Nếu sử dụng kèm thêm giờ, cần sử dụng theo cú pháp có kèm `T` hoặc 1 space giữa giờ và ngày

```
<time datetime="2018-08-23 11:00:00">August 23, 2018 at 11 am</time>
<time datetime="2018-08-23T11:00">August 23, 2018 at 11 am</time>
```

2.3. Có thể bổ sung thêm timezone của khu vực vào thời gian

```
<time datetime="T10:30-08:00">10:30am</time>
<time datetime="17:00-08:00">5:00 pm</time>
```

## Tài liệu liên quan

- https://www.w3schools.com/TAGS/att_time_datetime.asp
- https://www.htmlgoodies.com/html5/time-for-an-html5-time-tag.html