# Day 4: Template trong Vue.js

## Các directive thông dụng

**`v-cloak`**

Attribute này sẽ bị xóa đi khi Vue instance được khởi tạo xong.

Sử dụng với CSS:

```css
[v-cloak] {
  display: none;
}
```

**`v-if/else-if/else`**

Ẩn hiện element bằng cách ẩn hẳn khỏi DOM (chỉ hiện comment)

```vue
<div class="card__label" v-if="label">{{ label }}</div>
<!--- Hiển thị trong trình duyệt khi inspector --->
<--------->
```

`v-if` + bind class thì class sẽ không chạy trên IE

```
template: `
  <p v-if="activate" :class="is-activate">{{ text }}</p>
`
```

**`v-show`**

Ẩn hiện element bằng CSS (display none/block)

```html
<div class="card" v-if="activateCard">
<!--- Hiển thị trong trình duyệt khi inspector --->
<div class="card" style="display: none;">
```

**`v-for`**

> Khi sử dụng `v-for` thì cần sử dụng `:key`

- Khi data thay đổi, Vue.js sẽ generate rồi tiến hành thay thế
- Khi không có key, toàn bộ element sử dụng for sẽ bị thay đổi => mất thời gian
- Khi có key, Vue.js sẽ đọc đúng được thành phần nào cần thay đổi
- Key bị duplicate sẽ gây ra lỗi

Dùng với biến `items` là 1 *Array*

```vuejs
<row v-for="(item, index) in items"></row>
```

Dùng với biến `item` là một *Object*

Trường hợp 1: chỉ lấy value (vd item1, item2, item3) như trong ví dưới đây.

```vuejs
data() {
  return {
    objectItems: {
      key1: 'item1',
      key2: 'item2',
      key3: 'item3'
    }
  }
},
template: `
  <ul>
    <li v-for="item in objectItems">{{ item }}</li>
  </ul>
`
```

Trường hợp 2: Lấy cả key và value (key1, item1...)

```vuejs
template: `
  <ul>
    <li v-for="(item, key, index) in objectItems">
      {{ item }} - {{ key }} - {{ index }}
    </li>
  </ul>
`
```

Trường hợp 3: Dùng với range (1 tập hợp số)

```vuejs
template: `
  <ul>
    <li v-for="item in 15">{{ item }}</li>
  </ul>
`
```

**`v-on:event`**

Sử dụng `v-on:click`, `v-on:change` hoặc viết tắt `@event:click`

> `v-on:click.prevent` => gọi đến `e.preventDefault()`

## Cách gọi element bất kỳ trong vue.js

Lấy root bằng `this.$el`

Lấy element con bằng việc sử dụng [`ref=""`](https://vuejs.org/v2/api/#ref). Cách này thay cho việc sử dụng `select('.js-class', el)` thường dùng trên các project ES5/ES6 thông thường.

```vuejs
methods: {
  activateDescription: function() {
    this.$refs.heroDescription.classList.add('is-active')
  }
},
template: `
  <p ref="heroDescription">{{ description }}</p>
`
```

> `$refs` are only populated after the component has been rendered, and they are not reactive. It is only meant as an escape hatch for direct child manipulation - you should avoid accessing `$refs` from within templates or computed properties.

## Cách bind class, styles

- Bind class bằng variable (data, computed), object, array

Bằng variable thông thường

```vuejs
data: {
  customClass: 'card--bg'
},
template: `
  <card :class="customClass"></card>
`
```


Bằng variable là dạng 1 array

```vuejs
data: {
  classObject: {
    active: true,
    'text-danger': false
  }
},
template: `
  <div v-bind:class="classObject"></div>
`
```

Bằng variable của `computed` (dynamic)

```vuejs
data: {
  isActive: true,
  error: null
},
computed: {
  classObject: function () {
    return {
      active: this.isActive && !this.error,
      'text-danger': this.error && this.error.type === 'fatal'
    }
  }
},
template: `
  <div v-bind:class="classObject"></div>
`
```

Dùng với component:

```vuejs
<card v-bind:class="{ 'card--active': isActive }"></card>
```
