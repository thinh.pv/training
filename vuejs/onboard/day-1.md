# Day 1: Giới thiệu về Vue.js

## Giới thiệu Vue.js

Vue.js hỗ trợ các trình duyệt nào? Vue.js chạy trên tất cả các trình duyệt có [hỗ trợ ES5, từ IE5](https://www.w3schools.com/js/js_versions.asp)

**Cách nhận biết 1 project có sử dụng Vue.js hay không?**

- Tìm file webpack config có tên `webpack.config.js` trong project
- Tìm theo từ khóa: `new Vue(` trong trình soạn thảo code (phpStorm)
- Tìm các file có đuôi `.vue` hay `.vue.js` trong trình soạn thảo code (phpStorm)

**Cách viết code thế nào cho đúng theo từng project**

- Kiểm tra file `lib/init.js` để biết được cách project đó gọi module như thế nào.
- Kiểm tra các module đã có sẵn (để lấy cấu trúc mẫu của module).
- Các data attribute thường dùng:

```
[app]
[app-module]
[app-component]
[app-vue]
```

## Nâng cao

Cách cài đặt Vue.js vào 1 project thực tế (ví dụ từ Barrel).

- Cài đặt qua npm: `npm install --save-dev vue`
- Chỉnh sửa file config `webpack.config.js` và thêm thiết lập cho VueJS depencency ![Webpack](https://gitlab.com/thinh.pv/training/uploads/f8c7cb189e2fcc4e43851d533fa24df0/webpack.png)
- Bên trong module cần sử dụng Vue.js, import theo cú pháp của ES6

```vuejs
import Vue from 'vue'
export default el => new Vue({
  el,
  mounted () {
    console.log('Hello World!')
  }
})
```
