# Day 2: Vue Instance là gì?

## Vue Instance là gì?

Vue Instance là 1 Class với tên gọi là `Vue`

```vuejs
class SampleClass {
  constructor (options) {
    this.el = options.el
  }

  showEl () {
    console.log(this.el)
  }
}

const sample = new SampleClass({
  el
})

sample.showEl()
```

##  Cách khởi tạo 1 Vue Instance / Class

```vuejs
const app = new Vue({
    // options
})
```

## Các tham số thường sử dụng trong Vue

```
    + el
        + *required
        + type: Element, ID.
        + func: Vue sẽ init 1 Instance với gốc (root) là element này.
    + data
        + type: function
        + func: Internal data sử dụng trong Instance
    + computed
        + type: Object (Object gồm nhiều functions)
        + func: Tạo thêm các biến, có khả năng tính toán.
    + methods
        + type: Object (Object gồm nhiều functions).
        + func: Tạo các function sử dụng bên trong Instance.
    + template
        + type: HTML (kết hợp với syntax đặc biệt của Vue.js)
        + func: Template cho Instance hiện tại
        + note: Có thể sử dụng inline-template
    + created
        + type: function
        + func: Function nằm bên trong `created` này sẽ chạy trước khi template được render
        + note: Các function cho ajax thường đặt trong này
    + mounted:
        + type: function
        + func: Function nằm bên trong `mounted` này sẽ chạy sau khi template được render
```

**Sử dụng `this`**

Do bản chất của Vue Instance là 1 Class nên tất cả các thuộc tính đều có thể truy cập được bằng this, this này tương ứng với Class hiện tại

**Không sử dụng arrow function**

Tại sao không nên sử dụng `arrow function` cho các loại computed, methods,... trong Vue.js?

Do đặc thù của arrow function trong ES6, sử dụng `this` bên trong 1 arrow function chỉ giới hạn bên trong scope của function đó, không thể truy cập ra bên ngoài.

```vuejs
fullName: () => `${this.first_name} ${this.last_name}` // Lỗi
```

Có thể sử dụng được arrow function cho các function hay variable nào không cần động đến các thuộc tính/variable khác

```vuejs
fullName: () => 'Luke Tran' // Không lỗi
```

**Cách viết tắt gọn hơn function**

```vuejs
methods: {
    fullName: function() {
        return this.firstName + ' ' + this.lastName;
    }
}
```

Có thể được viết thành:

```vuejs
methods: {
    fullName () {
        return this.firstName + ' ' + this.lastName;
    }
}
```

## Nâng cao

### `$nextTick` để làm gì, cách hoạt động như thế nào?

Không được sử dụng thường xuyên. Có thể dựng cả page, chỉ cần sử dụng 1-2 lần.

Nguyên tắc hoạt động của Vue khi render data:

- Ban đầu Vue sẽ lấy data của vue để generate 1 bản HTML.
- Các thay đổi tiếp theo sẽ được đẩy vào hàng đợi
- Data sẽ được replace vào các vị trí đúng trong template
- DOM được thay thế lại vào
- Thường thì sẽ chạy tốc độ rất nhanh đến mức không nhận biết được khi nhìn trong Inspector.

```vuejs
export default el => new Vue({
    el,
    data () {
        return {
            message: 1
        }
    },
    mounted () {
        this.message = 2
        console.log(this.$el.textContent)
        this.$nextTick(function () {
          console.log(this.$el.textContent)
        })
    },
    template: `
       <p>{{ message }}</p>
    `
})
```

**Trường hợp thực tế:**

    + Cần làm 1 module `product grid`, cứ hiển thị hơn 4 items thì chuyển thành **carousel**, nếu không sẽ là grid.
    + Vấn đề ở đây là không biết lúc nào tất cả các item được load vì các item này sẽ nằm ở 1 component khác.
    + `$nextTick` giải quyết được vấn đề này. Chỉ cần cho **Carousel** vào bên trong **nextTick** là ổn. 

Tham khảo thêm [Sơ đồ của 1 Vue Instance](https://vuejs.org/v2/guide/instance.html#Instance-Lifecycle-Hooks)
