# Day 3: Tìm hiểu về data + Cú pháp (Syntax) trong template của Vue.js

**Khái niệm**

- Template của Vue.js dùng HTML kết hợp với các cú pháp (syntax) đặc trưng để tạo thành template.
- Do là chuỗi HTML nên nó khá dễ đọc, dễ hiểu cho developers.

**Cách viết template cho 1 module trong project thực tế?**

- Template bên trong file JS
- Inline template
- Single file component

## Syntax cơ bản trong Vue.js

### Cách in 1 giá trị (data/computed)

- Sử dụng cặp dấu ngoặc nhọn: `{{ }}` để in giá trị trong Vue

```vuejs
<div>{{ title }}</div>
```

- Sử dụng trong Shopify (Liquid) cần wrap code trong `{% raw %}{% endraw %}`

```vuejs
{% raw %}
<div>{{ title }}</div>
{% endraw %}
```

- Có thể sử dụng Javascript bên trong hai dấu ngoặc nhọn này. Tuy nhiên không nên lạm dụng quá, đôi khi sẽ gây lỗi khi sử dụng 1 function không hỗ trợ các trình duyệt cũ cũng như gây rối logic.

```vuejs
<div>{{ isAvailable ? "Add to cart" : "Sold out" }}</div>
<div :class="{ 'hero--title': hasTitle }">
```

- Không nên viết qúa nhiều logic bên trong template, các biến nào cần xử lý thì cho vào computed

```vuejs
Vue.component('row', {
  computed: {
    isActive: {
      get: function() {
        return this.selected.includes(this.id)
      },
      set: function() {
        return this.selected.includes(this.id)
      }
    }
  },
  template: `
    <div class="row" :class="{ 'row--active': isActive }">
  `
})
```
 
- Cách in HTML: sử dụng [`v-html`](https://vuejs.org/v2/guide/syntax.html#Raw-HTML)

```vuejs
data: function() {
    return {
        rawHtml: `<p style="color: red">The red text</p>`
    }
},
template: `
  <p>Using v-html directive: <span v-html="rawHtml"></span></p>
`
```

### Cách thêm/sửa 1 attribute của element

Khi muốn sửa attribute của 1 thẻ HTML, ví dụ: style, class, disabled,... Sử dụng `v-bind:attribute` hoặc `:attribute` (viết tắt).

```vuejs
<label class="form__label" :for="id">{{ title }}</label>
```

### Sử dụng data + computed + methods

**Khác nhau giữa `data` và `computed`**

- `data` là biến ban đầu
- `computed` là biến được tính toán dựa vào data 
- `data` là variable còn `computed` là function (có logic)
- `computed` sử dụng trong input cần binding 2 chiều, nên cần có `get` và `set` cho mỗi variable bên trong.

```vuejs
data: function() {
    return {
        title: null
    }
},
computed: {
    isActive: {
		get: function() {
			return this.selected.includes(this.id)
		},
		set: function() {
				return this.selected.includes(this.id)
			}
		},
	}
}
```

**Điểm khác nhau giữa computed và methods**

- `computed` không nhận param đầu vào
- computed có thể gọi như 1 variable (this.computed) mặc dù nó là function
- `computed` được coi như 1 giá trị `động` - nó sẽ tự động cập nhật giá trị khi data mà nó đang sử dụng thay đổi. Thực tế nó được lưu lại vào bộ nhớ cùng với `data` => tốc độ nhanh hơn.

```vuejs
computed: {
	updateCheckbox: {
		get() {
			return this.isActive
		},
		set(newValue) {
			if (newValue) {
				this.$emit('increase', this.id)
			} else {
				this.$emit('decrease', this.id)
			}

			this.isActive = newValue
		}
	}
}
```

- `methods` là function. Nó chỉ chạy khi được gọi vào 1 event nào đó.
- `methods` phải gọi như 1 function

```vuejs
methods: {
    loadFormatPrice(value) {
        return formatPrice(value) + window.currency
    }
},
template: `
    <div class="price">{{ loadFormatPrice(price) }}</div>
`
```
