# Commit trong Git ✅

## Tên commit

Quy ước:

```
[FIRST_ID] [SECOND_ID] [VERB] + [SOMETHING] + [WHERE]
```

- FIRST_ID: có thể có hoặc không, là mã số hiển thị trong YouTrack ở cuối cột phải ticket
- SECOND_ID: luôn luôn có, là ticket ID nằm trong đường dẫn YouTrack
- VERB: là động từ nguyên thể (không chia)
- SOMETHING: là danh từ, thường là đối tượng của hành động VERB
- WHERE: trạng ngữ, mô tả vị trí (trang, module) đã sửa

**Lưu ý:** SOMETHING có thể chứa tính từ hoặc danh từ bổ nghĩa, thành tập hợp mô tả tính chất sự vật.

Ví dụ:

```
JCB-21 JPD-35 Fix global typography heading 4
MAR-21 Update hero title color in page About
BARR-21 Add module Two Up to page Services
GHO-21 Fix query var in News Rest API
LLO-21 Add ACF fields for page About
ATM-21 Create a template News landing
```

## Chỉ commit file trước khi compile

Các file không commit vì nó được tự tạo bởi compiled tool như Webpack, Gulp, GruntJS

```
*.min.css
*.min.js
```

Các file tạo bởi IDE (trình soạn thảo) không nên commit

```
.vscode/
.idea/
```

## Commit đúng nội dung và tách nhỏ commit

Đang sửa các file nào với mục đích gì thì commit các file đó.
Tách commit để dễ quan sát và khôi phục các phần làm sai hoặc gỡ bỏ commit.

```
# Add module Two Up
+ modules/two-up/two-up.php
+ modules/two-up/two-up.css

# Add global typography two up title
+ src/css/base/typography.css

# Add global color two up
+ src/css/base/colors.css

# Add Two Up ACF fields in Homepage
+ acf-json/group_4253423124234.json
```