# Git Flow ✅

Git Flow là những quy ước nhằm đảm bảo các branch và Merge Request được tạo đúng theo quy ước tên và checkout đúng gốc ban đầu.

## Git Flow quy ước

- master: nhánh chính, dùng để release hoặc triển khai fix nóng
- develop: nhánh chính, dùng để triển khai tính năng mới hoặc fix bug
- bugfix/NAME: nhánh tạo ra hoặc checkout từ nhánh **develop** cho mục đích **fix bug**
- feature/NAME: nhánh tạo ra hoặc checkout từ nhánh **develop** cho mục đích làm **tính năng mới**
- hotfix/NAME: nhánh tạo ra hoặc checkout từ nhánh **master** cho mục đích **fix nóng** trên môi trường live

## Đặt tên nhánh (NAME)

Tên nhánh có thể đặt theo nhiều quy tắc, nhưng gợi ý dưới đây là các quy tắc phổ biến tùy theo sự phân công gói công việc của từng dự án.

**Đặt tên theo module**

Mỗi module/component nằm độc lập trên 1 branch.
Mỗi developer làm riêng một nhánh này và thường xuyên được leader merge commit vào nhánh `develop`

```
feature/hero
feature/accordion
feature/two-up
```

**Đặt tên theo template**

Một template có thể nhiều người làm.
Đặt tên theo template sẽ yêu cầu mọi người cùng làm 1 branch và rebase liên tục trước khi đẩy lên branch.
Cách khác là leader liên tục merge commit vào nhánh này, và mọi người rebase liên tục trên nhánh này.

```
# Main Feature Branch
feature/about-page

# Developer's Branches
feature/about/hero
feature/about/two-up
feature/about/bottom-cta
```

> Lưu ý: không nên đặt tên nhánh của developer là `feature/about-page/hero` vì có thể bị GitLab không cho đẩy code lên.

**Đặt tên theo thời gian**

Trường hợp nhiều vấn đề trên các page khác nhau trong cùng một project, developer chủ động tạo branch theo cấu trúc:

```
bugfix/<NAME>/<MMDD>
```

Ví dụ:

```
bugfix/phuong/1201
bugfix/khoi/0415
```
