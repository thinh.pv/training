# Setup git khi làm việc với các project bên Vaulted Oak ✅

# Cách setup
- Clone lại các projects bên Vaulted Oak vào folder "vaulted-oak"
- Tạo mới file `~/.vaulted-oak.git.inc` (ở thư mục `~` của máy chứ không phải root của project) với nội dung như sau:
```
[user]
    name = Vaulted Oak Dev
    email = gary.cao@vaulted-oak.com
```
- Tạo hoặc tìm file `~/.gitconfig`
- Thêm config sau vào cuối file `.gitconfig`
```
[includeIf "gitdir:vaulted-oak*/"]
	path = "~/.vaulted-oak.git.inc"
[includeIf "gitdir:voak*/"]
	path = "~/.vaulted-oak.git.inc"
```

# References:
- Docs: https://git-scm.com/docs/git-config#_conditional_includes
- Glob: https://www.linuxjournal.com/content/globbing-and-regex-so-similar-so-different
- Test glob pattern: https://globster.xyz/
