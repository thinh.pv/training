# Setup git khi làm việc với các project bên Barrel ✅
# Mục đích:
- Giữ nguyên tên của developers khi push code sang git của Barrel. Theo đó lược bỏ được 1 phần công việc cho các Unit leader hàng ngày khi đẩy code sang Barrel
- Email khi push sẽ có dạng `name@barreldev.com`

# Cách setup
- Tạo mới file `~/.barrel.git.inc` (ở thư mục `~` của máy chứ không phải root của project) với nội dung như sau:
```
[user]
    name = Name
    email = name@barreldev.com
```
- Trong đó: `Name` là Tên tiếnh anh, ví dụ:
```
[user]
	name = Luke Tran
	email = luke.tran@barreldev.com
```
- Tạo hoặc tìm file `~/.gitconfig`
- Thêm config sau vào cuối file `.gitconfig`
```
[includeIf "gitdir:barr*/"]
	path = "~/.barrel.git.inc"
[includeIf "gitdir:brr*/"]
	path = "~/.barrel.git.inc"
```

# References:
- Docs: https://git-scm.com/docs/git-config#_conditional_includes
- Glob: https://www.linuxjournal.com/content/globbing-and-regex-so-similar-so-different
- Test glob pattern: https://globster.xyz/
