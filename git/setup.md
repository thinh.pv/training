# Setup a Git

## Install Git

Install via Homebrew

```
brew install git
```

## Setup Git Config Global

```
git config --global user.email "youremail@gmail.com"
git config --global user.name "Kevin Nguyen"
```

## Setup Global .gitignore

```
git config --global core.excludesfile ~/.gitignore
```
