# Tìm hiểu về API

API là cách để các phần mềm giao tiếp với nhau. Nó khác với "Human Interface", là cách ta tương tác với phần mềm ở vai trò người dùng.

Để triển khai API, có nhiều thuật ngữ cần nắm được, nhưng về bản chất xoay quanh việc sử dụng HTTP Request để chuyển dữ liệu nào đó đi và nhận request trả về.

Lấy ví dụ:

Một tính năng gần đây là cần làm sao để khi trong email gửi tới người dùng, vd `wanderlust.com/?adduser=hi@gmail.com`, điều này sẽ được xử lý như sau:

**Đọc `$_GET['adduser']` và xử lý dữ liệu, vd có phải email không**

```
function wl_proceed_add_user_query() {
  $_email_query= $_GET['adduser']; // Lấy giá trị của query từ URL ra thôi mà!
}

add_action('init', 'wl_proceed_add_user_query');
```

## Action & Hooks

> Xử lý dữ liệu trước khi thực thi là cách ngăn chặn các lỗi bảo mật, hoặc trả về sớm để tránh tốn tài nguyên.

Khi `add_action('init')` thực chất giống như ta muốn sử dụng function này "ở giai đoạn nào" của quá trình load. Ở đây, là khi [`init`](https://developer.wordpress.org/reference/hooks/init/)

```
Most of WP is loaded at this stage, and the user is authenticated. WP continues to load on the ‘init’ hook that follows (e.g. widgets), and many plugins instantiate themselves on it for all sorts of reasons (e.g. they need a user, a taxonomy, etc.).
```

Có nhiều giai đoạn, hoặc "vị trí". VD:

- Muốn load vào thẻ `<head>`, ta gọi đến [`wp_head`](https://developer.wordpress.org/reference/hooks/wp_head/)
- Muốn load vào thời điểm bắt đầu load WP, ta dùng hook [`wp`](https://developer.wordpress.org/reference/hooks/wp/)
- Muốn can thiệp vào thời điểm trước khi một bài viết được save, ta dùng hook [`save_post`](https://developer.wordpress.org/reference/hooks/save_post/)

Các hook là cách WordPress giúp ta không phải sửa trực tiếp core WordPress, mà can thiệp (insert code) vào trước hoặc sau khi các sự kiện diễn ra.

## Xử lý dữ liệu

Đừng để những gì bạn thấy bị đánh lừa, đó là tiêu chuẩn cao nhất khi xử lý input. Nếu bạn muốn đầu vào phải là số, string, object, hãy kiểm tra trước khi cho dữ liệu đấy chạy qua.

Đừng như thế này:

```
function wl_proceed_time($query_vars) {
  return $query_vars[0];
}
```

Khó có thể cam kết `$query_vars` luôn luôn là 1 Array. Hãy kiểm tra dữ liệu đầu vào bằng vài cách:

**Comment** là cách hiệu quả sẽ giúp trên IDE hiển thị dữ liệu nhập vào có thể sai định dạng của function đầu vào yêu cầu.

```
/**
* $query_vars: Array
**/
function wl_proceed_time($query_vars) {}
```

Cách khác là trong function nên return sớm, chia tay bớt khổ đau

```
function wl_proceed_time($query_vars) {
  if(!is_array($query_vars)) { return; }

  return $query_vars[0];
}
```
