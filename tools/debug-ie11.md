# Debug trên IE11

- Shoptify IE Test (use IP address and Preview theme ID)
    + Example: https://192.168.1.53:3000/?preview_theme_id=79490580542

- Wordpress IE Test:
    + Install `npm i localtunnel` see at link `https://www.npmjs.com/package/localtunnel`
    + Or Use IP Address same with shoptify
