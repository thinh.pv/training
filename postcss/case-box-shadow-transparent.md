# Box-shadow PNG transparent

Sử dụng box-shadow sẽ không đổ được bóng theo viền của ảnh nếu ảnh có đường cong.

![box-shadow issue](https://gitlab.com/thinh.pv/best-practices/uploads/5d65e1d674a39b8341979387512ab22c/box-shadow-issue.png)

## Cách xử lý

Trong trường hợp này ⇒ sử dụng `filter: drop-shadow`

```css
filter: drop-shadow(8px 8px 5px rgba(43, 21, 39, 0.15));
```

Công thức:

```css
filter: drop-shadow(
  <offset-x> <offset-y> <blur-radius> <spread-radius> <color>
);
```

- `<offset-x>`, `<offset-y>` (phải có): xác định độ dài của bóng theo phương ngang và dọc.
- `<blur-radius>` (tuỳ chọn): độ mờ của bóng, giá trị này càng lớn, bóng càng mờ, không được set giá trị âm, giá trị mặc định là 0 (bóng rõ nhất).
- `<spread-radius>` (tuỳ chọn): độ mở rộng của bóng. Bóng to hơn nếu giá trị này dương, bóng co lại nếu giá trị âm. Giá trị mặc định là 0. Webkit và một vài trình duyệt khác không hỗ trợ `<spread-radius>`.
- `<color>` (tuỳ chọn): quy định màu của bóng. Nếu không được set, `<color>` phụ thuộc vào trình duyệt (thường là màu đen).

### Các browser không hỗ trợ drop-shadow

**⇒ Phương án thay thế trên Opera và Firefox:**

```css
filter: url(drop-shadow.svg#drop-shadow);
```

```html
<svg height="0" xmlns="http://www.w3.org/2000/svg">
  <filter id="drop-shadow">
    <feGaussianBlur in="SourceAlpha" stdDeviation="2.2" />
    <feOffset dx="1" dy="4" result="offsetblur" />
    <feFlood flood-color="rgba(0,0,0,0.3)" />
    <feComposite in2="offsetblur" operator="in" />
    <feMerge>
      <feMergeNode />
      <feMergeNode in="SourceGraphic" />
    </feMerge>
  </filter>
</svg>
```

**Phương án thay thế trên IE11:**

```css
-ms-filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=1, Color='#444')";
filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=1, Color='#444')";
```

## Đọc thêm

- [MDN](<https://developer.mozilla.org/en-US/docs/Web/CSS/filter#drop-shadow()>)
