# postCSS General Coding Conventions

## Khoảng cách giữa các selector

1. Từ class tới dấu `{` cần 1 space. Viết cùng dòng.

```postcss
.hero__title {}
```

2. Từ selector này tới selector khác cần xuống dòng.

```postcss
.hero__subtitle {}
.hero__title {}
```

3. **Trong selector**, tuân theo các quy ước sau đây:

3.1. Thuộc tính của selector cha tới **selector con tiếp theo** HOẶC **@media** cần 1 line space:

```postcss
.hero--media {
  position: relative;

  .hero__title {}
}

.callout {
  display: block;

  @media (--m) {
    display: none;
  }
}
```

> Trường hợp class cha không có thuộc tính nào, viết liền luôn.

```postcss
.hero--media {
  .hero__title {}
}

.two-up--cta {
  .two-up__block {
    width: 50%;
  }
}
```

3.2. Giữa các lớp @media hay selector con thì không cần khoảng cách. Ưu tiên @media thuộc tính của parent rồi mới đến các selector con.

```postcss
.hero--media {
  position: relative;

  @media (--m) {}
  @media (--l) {}
  .hero__title {}
}
```

3.3. Nhiều selector cha/con cùng chung 1 breakpoint thì tách lớp theo thứ tự như sau:

```postcss
.parent {
  font-weight: 700;
  
  @media (--m) {}
  .child {
    @media (--m) {}
  }
}
```

4. Nhiều selector cha/con cùng chung thuộc tính thì viết xuống dòng cho mỗi selector.

```
.h4,
%h4 {
  font-size: 1.625rem;
  line-height: 1.75;
}
```

## Thứ tự thuộc tính

Thứ tự thuộc tính được quy ước viết theo mức ưu tiên như sau:

- Extends and mixin (SCSS/postCSS) - cao nhất
- Positioning
- Box model
- Visual

Ví dụ:

```
  /* Keep extends and mixins in the top */
  @extend .section;
  @include transition;
  /* Then declarations */

  /* Positioning */
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 100;

  /* Box-model */
  display: flex;
  width: 100px;
  height: 100px;

  /* Typography - thường cái này là class có sẵn và cho vào markup hoặc sử dụng @extend */
  font: normal 13px "Helvetica Neue", sans-serif;
  line-height: 1.5;
  color: #333;
  text-align: center;

  /* Visual */
  background-color: #f5f5f5;
  border: 1px solid #e5e5e5;
  border-radius: 3px;

  /* Misc */
  opacity: 1;
```

## Không cần prefix cho nhiều trình duyệt khi code

Không cần thêm prefixer cho các trình duyệt. Công việc này do webpack/gulp hỗ trợ khi compile code.

Chỉ thêm nếu phải code mà không có compiled tool trên project.

```
.hero {
    display: flex;
    /* Don't need add display: -webkit-flex; */
}
```

## Không ghi đơn vị đối với các trường hợp giá trị thuộc tính là `0`, ví dụ `0px`

```
.three-up {
  margin: 0;
  padding: 0;
}
```

## Nếu chỉ cần ghi đè một thuộc tính trên `@media` hoặc 1 class khác, không viết lặp lại code.

```
.two-up {
  padding: 25px 0;
}

.two-up--video {
  // Only bottom
  padding-bottom: 20px;
}
```

## Không viết tắt trừ khi cần thiết

Không đặt duy nhất 1 giá trị cho `margin` hay `padding`. Sử dụng ít nhất 2 giá trị nhằm tránh việc gán spacing thừa.

Các spacing nên thống nhất giữa các element, ví dụ cùng top spacing hoặc bottom spacing.

```postcss
margin: 10px auto;
margin: 0 10px 0 0;
```

## Sử dụng `content: none` thay vì `display: none`

```postcss
.hero {
  &:after {
    content: '';
    ...
  }
}

.hero--no-carousel {
  &:after {
    content: none;
  }
}
```

## Nesting class

Viết cấu trúc từ selector cha sang selector con trong module.

```postcss
.hero--media {
  .hero__title {}
}
```

Trường hợp đặc biệt: Các **class body** hoặc **nằm ngoài module** có thể viết lồng trong child element như sau: 

```postcss
.hero__title {
  .is-animation-active & {}
}
```

## Không viết nesting class theo element khi ngoài markup có thể thêm class

```
.hero--flexible {}
.two-up--flexible {}
```

~~Ví dụ không đúng:~~

```
.page-template-flexible {
  .hero {}
  .two-up {}
}
```

> Trường hợp ngoại lệ: khi content là do client nhập vào trong Post/Page/Field Editor không chứa class. Thường sử dụng `.wysiwyg` class để bọc ngoài và làm style.

Ví dụ: 


```
.wysiwyg {
  h2 {}
  h3 {}
  p {}
  ul {
    li {}
  }
}
```

## Extend class từ class gốc thì luôn viết ngay phía dưới class gốc.

```
.button {}
.button--red {}
.button--blue {}
```

## Không viết tắt tên của selector

Tránh việc đọc hiểu khó và không thể tái sử dụng bởi người khác.

```
.button {}
.tweet {}
```

~~Ví dụ không đúng:~~

```
.btn {}
.twt {}
```