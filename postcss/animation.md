# postCSS | Animation

**Không sử dụng transition với các thuộc tính như `padding` và `margin`** vì nó bị giật và thường không được client khuyến nghị sử dụng.

**Sử dụng `translate3d()`** để render sử dụng GPU giúp animation mượt hơn.

```postcss
.selector {
  transform: translate3d(0, -50%, 0);
  transition: transform .3s ease;
  &:hover {
    transition: translate3d(0, 0, 0);
  }
}
```

**Hạn chế sử dụng `transition: all`**

Cần áp dụng transition cho đúng thuộc tính, tránh set `all` không tối ưu.