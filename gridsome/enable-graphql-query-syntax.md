# Cách enable graphql syntax trong PHPStorm cho Gridsome query

1. Cài đặt Js Graphql plugin
   ![Settings](https://gitlab.com/thinh.pv/training/-/raw/a6d93ae37ac2c2f3dcd2ccefd6d48f98c861bd70/gridsome/install-plugin.png)
2. PhpStorm > Preference > Editor > Language injections
3. Nhập cho page-query và static-query
   ![Settings](https://gitlab.com/thinh.pv/training/-/raw/a6d93ae37ac2c2f3dcd2ccefd6d48f98c861bd70/gridsome/settings-query1.png)
   ![Settings](https://gitlab.com/thinh.pv/training/-/raw/a6d93ae37ac2c2f3dcd2ccefd6d48f98c861bd70/gridsome/settings-query2.png)
